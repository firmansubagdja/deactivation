<?php

namespace App\Http\Controllers;

use DB;
use App\Deacts;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $deacts = deacts::all();
        $sr_created  = deacts::count();
        $so_created  = deacts::whereNotNull('so_catalist')->count();
        $so_complete = deacts::whereNotNull('so_created_so_complete')->count();

        $ctd_alert = deacts::select(DB::raw(" 
                            COUNT(CASE
                            WHEN DATEDIFF(ctd_date, now()) > 0 AND pic='BRA' THEN id
                            WHEN DATEDIFF(ctd_date, now()) <= 0 AND DATEDIFF(ctd_date, now()) <= -6 AND pic='BRA' THEN id
                            WHEN DATEDIFF(ctd_date, now()) <= -7 AND DATEDIFF(ctd_date, now()) < -14 AND pic='BRA' THEN id
                            WHEN DATEDIFF(ctd_date, now()) <= -14 AND pic='BRA' THEN id
                            ELSE NULL
                            END) as alert_bra, 

                            COUNT(CASE
                            WHEN DATEDIFF(ctd_date, now()) > 0 AND pic='SAD' THEN id
                            WHEN DATEDIFF(ctd_date, now()) <= 0 AND DATEDIFF(ctd_date, now()) <= -6 AND pic='SAD' THEN id
                            WHEN DATEDIFF(ctd_date, now()) <= -7 AND DATEDIFF(ctd_date, now()) < -14 AND pic='SAD' THEN id
                            WHEN DATEDIFF(ctd_date, now()) <= -14 AND pic='SAD' THEN id
                            ELSE NULL
                            END) as alert_sad"))
                    ->get();

        $journey_customer = deacts::select(DB::raw("month_sr, 
                COUNT(IF(journey_customer='Cust Late Req' AND status_catalist='Complete', id, NULL)) as cust_late_complete, 
                COUNT(IF(journey_customer='Cust Late Req' AND status_catalist='In Progress', id, NULL)) as cust_late_inprog,
                COUNT(IF(journey_customer='Cust Not Late Req' AND status_catalist='Complete', id, NULL)) as cust_not_late_complete, 
                COUNT(IF(journey_customer='Cust Not Late Req' AND status_catalist='In Progress', id, NULL)) as cust_not_late_inprog"))
                    ->groupBy('month_sr')
                    ->get();

        $month_act_journey = deacts::select('month_sr')
                ->groupBy('month_sr')
                ->get();

        $sr_created_act_journey = deacts::select('*')
                                ->groupBy('month_sr')
                                ->count();

        // dd($ctd_alert);
        //dd($sr_created_act_journey);
        return view('layouts.pages.index',compact('sr_created', 'so_created', 'so_complete', 'ctd_alert', 'journey_customer', 'month_act_journey')); 
    }
}
