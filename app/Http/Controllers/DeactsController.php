<?php

namespace App\Http\Controllers;

use App\Deacts;
use App\SystemLogs;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DeactsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('layouts.pages.deacts.index');
        $deacts = DB::table('deacts')->orderBy('id', 'DESC')->paginate(10);
        //dd($deacts);
        return view('layouts.pages.deacts.index',compact('deacts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.pages.deacts.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin'){

            $datenow = Carbon::now();

            $deacts = new Deacts(); 
            $deacts->sr_penanda            = request('sr_penanda');
            $deacts->sr_catalist           = request('sr_catalist');
            $deacts->so_number             = request('so_number');
            $deacts->cid                   = request('cid');
            $deacts->customer              = request('customer');
            $deacts->ctd_date              = request('ctd_date');
            $deacts->create_date_assign    = $datenow;
            $deacts->request_date_sales    = request('request_date_sales');
            $deacts->request_date_cust     = request('request_date_cust');
            $deacts->product_type          = request('product_type');
            $deacts->quantity_cid          = request('quantity_cid');
            $deacts->channel_request       = request('channel_request');
            $deacts->email_requestor       = request('email_requestor');
            $deacts->reason_midi           = request('reason_midi');
            $deacts->sub_reason1           = request('sub_reason1');
            $deacts->sub_reason2           = request('sub_reason2');
            $deacts->sub_reason3           = request('sub_reason3');
            $deacts->reason_validasi       = request('reason_validasi');
            $deacts->pic_sr_penanda        = request('pic_sr_penanda');
            $deacts->keterangan            = request('keterangan');
            $deacts->pic                   = 'BRA';
            

            $deacts->save();

            SystemLogs::create([
                'sr_penanda'         => request('sr_penanda'),
                'pic'                => 'BRA',
                'deact_id'            => $deacts->id,
                'user_id'            => Auth::user()->id,
                'status'             => 0,
            ]);

        }

        return redirect()->route('deacts')
        ->with('success','Data created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deacts  $deacts
     * @return \Illuminate\Http\Response
     */
    public function show(Deacts $deacts)
    {
        return view('deacts.show',compact('deacts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Deacts  $deacts
     * @return \Illuminate\Http\Response
     */
    public function edit(Deacts $deacts, $id)
    {
        $deacts = deacts::where('id', $id)->first();
        return view('layouts.pages.deacts.edit',compact('deacts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deacts  $deacts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deacts $deacts, $id)
    {
        $request->validate([
            // 'sr_penanda' => 'required',
            // 'sr_catalist' => 'required',
            // 'cid' => 'required',
            // 'customer' => 'required',
            // 'ctd_date' => 'required',
            // 'create_date_assign' => 'required',
            // 'request_date_sales' => 'required',
            // 'request_date_cust' => 'required',
            // 'product_type' => 'required',
            // 'channel_request' => 'required',
            // 'email_requestor' => 'required',
            // 'reason_midi' => 'required',
            // 'sub_reason1' => 'required',
            // 'sub_reason2' => 'required',
            // 'sub_reason3' => 'required',
            // 'reason_validasi' => 'required',
            // 'pic_sr_penanda' => 'required',
            // 'keterangan' => 'required'
            // 'so_catalist' => 'required',
            // 'so_created' => 'required',
            // 'reason_catalist' => 'required',
            // 'ctd_update' => 'required',
            // 'termination_date' => 'required',
            // 'status_catalist' => 'required',
            // 'completion_date' => 'required',
            // 'discrepancy' => 'required',
            // 'delay_completion_reason' => 'required',
            // 'note' => 'required',
            // 'pic_sd' => 'required',
            // 'mrc' => 'required',
            // 'month_sr' => 'required',
            // 'ctd_alert' => 'required',
            // 'ctd_vs_termination' => 'required',
            // 'flagging_ctd_vs_termination' => 'required',
            // 'journey_customer' => 'required',
            // 'journey_sales' => 'required',
            // 'delay_request' => 'required',
            // 'request_ioch' => 'required',
            // 'sr_so_created' => 'required',
            // 'so_created_so_complete' => 'required'
        ]);

        if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin'){

            Deacts::where('id', $id)->update([  
             'sr_penanda'            => request('sr_penanda'),
             'sr_catalist'           => request('sr_catalist'),
             'so_number'             => request('so_number'),
             'cid'                   => request('cid'),
             'customer'              => request('customer'),
             'ctd_date'              => request('ctd_date'),
             'request_date_sales'    => request('request_date_sales'),
             'request_date_cust'     => request('request_date_cust'),
             'product_type'          => request('product_type'),
             'channel_request'       => request('channel_request'),
             'email_requestor'       => request('email_requestor'),
             'reason_midi'           => request('reason_midi'),
             'sub_reason1'           => request('sub_reason1'),
             'sub_reason2'           => request('sub_reason2'),
             'sub_reason3'           => request('sub_reason3'),
             'reason_validasi'       => request('reason_validasi'),
             'pic_sr_penanda'        => request('pic_sr_penanda'),
             'keterangan'            => request('keterangan')
            ]);       

        }

        $get_data = Deacts::where('id', $id)->first();

        $pic ="";

        if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin'){
            if ($get_data->status_catalist == null) {
                $pic = "SAD";
            }
            
            Deacts::where('id', $id)->update([ 
             'so_catalist'       => request('so_catalist'),
             'so_created'        => request('so_created'),
             'so_submission'     => request('so_submission'),
             'reason_catalist'   => request('reason_catalist'),
             'ctd_update'        => request('ctd_update'),
             'termination_date'  => request('termination_date'),
             'mrc'               => request('mrc'),
             'pic'               => $pic         
            ]);         

        }
        
        // $formattedweddingdate = $myDateTime->format('d-m-Y');
        // $myDateTime = DateTime::createFromFormat('Y-m-d', $weddingdate);

        // dd($get_data->create_date_assign);
        $get_data = Deacts::where('id', $id)->first();

        $ctd_carbon = new Carbon($get_data->ctd_date);
        $datenow = Carbon::now();
        
        $month_sr = date("Y-m", strtotime($get_data->create_date_assign));
        $ctd_vs_termination = '';
        if(strtotime($get_data->termination_date) == ''){
            $ctd_vs_termination = null;
        }else{
            $ctd_vs_termination = round((strtotime($get_data->termination_date) - strtotime($get_data->ctd_date)) / 86400);
        }

        $flagging_ctd_vs_termination = '';
        if(strtotime($get_data->create_date_assign) == ''){
            $flagging_ctd_vs_termination = '';
        }else if(is_null($ctd_vs_termination)){
            $flagging_ctd_vs_termination = 'Not Inputed yet by BRA';
        }else if($ctd_vs_termination == 0 ){
            $flagging_ctd_vs_termination = 'No Impact';
        }else if($ctd_vs_termination <= 2){
            $flagging_ctd_vs_termination = 'No Impact';
        }else if($ctd_vs_termination > 2){
            $flagging_ctd_vs_termination = 'Potensial Adjustment';
        }

        $journey_customer = '';
        if(strtotime($get_data->ctd_date) == ''){ 
            $journey_customer ='';
        }else if(strtotime($get_data->ctd_date) - strtotime($get_data->request_date_cust) > 7){
            $journey_customer='Cust Not Late Req';
        }else if(strtotime($get_data->ctd_date) - strtotime($get_data->request_date_cust) <= 7){
            $journey_customer='Cust Late Req';
        }

        $journey_sales = '';
        if(strtotime($get_data->request_date_sales) == ''){ 
            $journey_sales ='';
        }else if (strtotime($get_data->request_date_cust) == ''){ 
            $journey_sales ='';
        }else{
            $journey_sales = round((strtotime($get_data->request_date_sales) - strtotime($get_data->request_date_cust)) / 86400);
        }

        $delay_request = '';
        if(strtotime($get_data->request_date_sales) == ''){ 
            $delay_request ='';
        }else if ($journey_sales > 0 ){ 
            $delay_request ='Delay';
        }else if ($journey_sales <= 0 ){
            $delay_request = 'No Delay';
        }

        $request_ioch = '';
        if(strtotime($get_data->request_date_sales) == ''){ 
            $request_ioch ='';
        }else { 
            $request_ioch = round((strtotime($get_data->create_date_assign) - strtotime($get_data->request_date_sales)) / 86400 );
        }

        $sr_so_created = '';
        if(strtotime($get_data->so_created) == ''){ 
            $sr_so_created ='';
        }else { 
            $sr_so_created = round((strtotime($get_data->so_created) - strtotime($get_data->create_date_assign)) / 86400 );
        }

        $so_created_so_complete = '';
        if(strtotime(request('completion_date')) == ''){ 
            $so_created_so_complete ='';
        }else { 
            $so_created_so_complete = round((strtotime(request('completion_date')) - strtotime($get_data->so_created)) / 86400 );
        }

        $ctd_mark = '';

        $aging = '';

        if(Auth::user()->dept == 'SAD' || Auth::user()->dept == 'admin'){
            if (request('status_catalist') == "In Progress") {
                $pic = 'SAD';
            } else if (request('status_catalist') == "Complete") {
                $pic = 'Complete';
            }

            Deacts::where('id', $id)->update([ 
             'status_catalist' => request('status_catalist'),
             'completion_date' => request('completion_date'),
             'discrepancy' => request('discrepancy'),
             'delay_completion_reason' => request('delay_completion_reason'),
             'note' => request('note'),
             'pic_sd' => Auth::user()->name,
             'month_sr' => $month_sr,
             'ctd_vs_termination' => $ctd_vs_termination,
             'flagging_ctd_vs_termination' => $flagging_ctd_vs_termination,
             'journey_customer' => $journey_customer,
             'journey_sales' => $journey_sales,
             'delay_request' => $delay_request,
             'request_ioch' => $request_ioch,
             'sr_so_created' => $sr_so_created,
             'so_created_so_complete' => $so_created_so_complete,
             'ctd_mark' => $ctd_mark,
             'pic' => $pic,
             'aging' => $aging
            ]);        
            
        }

        SystemLogs::where('sr_penanda', $get_data->sr_penanda)->where('deact_id', $id)->where('pic', Auth::user()->dept)->update([
                'sr_penanda'         => $get_data->sr_penanda,
                'pic'                => $pic,
                'deact_id'           => $id,
                'user_id'            => Auth::user()->id,
                'status'             => 0,
                'processed_at'       => $datenow,
        ]);  
        SystemLogs::create([
                'sr_penanda'         => $get_data->sr_penanda,
                'pic'                => $pic,
                'deact_id'           => $id,
                'user_id'            => Auth::user()->id,
                'status'             => 0,
            ]);

        return redirect()->route('deacts')
        ->with('success','Data update successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deacts  $deacts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deacts $deacts)
    {
        //
    }
}
