<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
    	if (Auth::check()) {
            return redirect('/dashboard');
        } else {
            return view('layouts.pages.login');
        }
    }

    public function login(Request $request)
    {
    	// dd($request->all());
    	$data = User::where('email', $request->email)->firstorfail();
    	if ($data) {
    		if (Hash::check($request->password,$data->password)) {

    			return redirect('/dashboard');

    		}
    	}
    	// return redirect('/')with->('message', 'Email atau Password Salah');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
