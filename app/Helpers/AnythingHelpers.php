<?php

function getNotifList($pic){
	$result = \App\SystemLogs::where('pic', '=', $pic)
    ->orderBy('id', 'desc')
    ->where('processed_at', null)
    ->get();

    return $result;
}

function getNotifCount($pic){
	$result = \App\SystemLogs::where('pic', '=', $pic)
    ->where('processed_at', null)
    ->count();

    return $result;
}


?>