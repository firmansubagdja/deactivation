<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deacts extends Model
{
	protected $fillable = [
		'sr_penanda', 'sr_catalist', 'so_number', 'cid', 'customer', 'ctd_date', 'create_date_assign', 'request_date_sales', 'request_date_cust', 'product_type', 'channel_request', 'email_requestor', 'reason_midi', 'sub_reason1', 'sub_reason2', 'sub_reason3', 'reason_validasi', 'pic_sr_penanda', 'keterangan', 'so_catalist', 'so_created', 'so_submission', 'reason_catalist', 'ctd_update', 'termination_date', 'status_catalist', 'completion_date', 'discrepancy', 'delay_completion_reason', 'note', 'pic_sd', 'mrc', 'month_sr', 'ctd_alert', 'ctd_vs_termination', 'flagging_ctd_vs_termination', 'journey_customer', 'journey_sales', 'delay_request', 'request_ioch', 'sr_so_created', 'so_created_so_complete','ctd_mark','pic','aging'
	];
}