<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@index')->name('login');
Route::post('login', 'AuthController@login')->name('login');
Route::get('login', 'AuthController@index')->name('login');
Route::get('logout', 'AuthController@logout')->name('logout');

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/navbar', 'NavbarController@index')->name('navbar');

// Route::get('/deacts', function () {
//     return view('layouts.pages.deacts.index');
// });
Route::get('/deacts', 'DeactsController@index')->name('deacts');
Route::get('/add_deacts', 'DeactsController@create')->name('add_deacts');
Route::get('/edit_deacts/{id}', 'DeactsController@edit')->name('edit_deacts');
Route::post('/store_deacts', 'DeactsController@store')->name('store_deacts');
Route::post('/update_deacts/{id}', 'DeactsController@update')->name('update_deacts');



Route::get('/sqa', 'SqaController@index')->name('sqa');
// Route::get('/sqa', function () {
//     return view('layouts.pages.sqa.index');
// });

Route::get('/rca', function () {
    return view('layouts.pages.rca.index');
});

Route::get('/sad', function () {
    return view('layouts.pages.sad.index');
});
