<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Deactivation | Dashboard</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url ('/adminlte/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/summernote/summernote-bs4.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{ url ('/adminlte/plugins/daterangepicker/daterangepicker.css')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <!-- <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div> -->

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>
      
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="{{ route('navbar') }}">
          <i class="far fa-bell"></i>
          <span class="badge badge-danger navbar-badge">{{ getNotifCount(Auth::user()->dept) }}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">{{ getNotifCount(Auth::user()->dept) }} Notifications</span>
          <?php 
            foreach (getNotifList(Auth::user()->dept) as $notif) {
          ?>
          <div class="dropdown-divider"></div>        
          <a href="{{ url('/edit_deacts/' . $notif->deact_id) }}" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> <?php echo $notif->sr_penanda; ?>
            <span class="float-right text-muted text-sm"><?php echo $notif->sr_penanda; ?></span>
          </a>

        <?php } ?>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-slide="true" href="{{ url('/logout') }}" role="button">
          <i class="fas fa-sign-out-alt"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
      <img src="{{ url('/adminlte/dist/img/deactivation.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"> <b>DEACTIVATION</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!-- <img src="{{ url('/adminlte/dist/img/user2-160x160.jpgf') }}" class="img-circle elevation-2" alt="User Image"> -->
          <img src="{{ url('/adminlte/dist/img/logoisat.png') }}" class="img-responsive" alt="Logo Indosat Ooredoo" style="width:205px">
        </div>
      </div>

      <!-- SidebarSearch Form -->
     <!--  <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url ('/home')}}" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard Home</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('deacts') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Deactivation
              </p>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Division
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/sqa" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Srv. Quality & Assurance</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/rca" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>BRA</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/sad" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Srv. Activation & Delivery</p>
                </a>
              </li>
            </ul>
          </li> -->
          
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  
    <!-- /.content-header -->

    <!-- Main content -->

    @yield('content')

    <!-- /.content -->
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href=""></a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ url ('/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ url ('/adminlte/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ url ('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{ url ('/adminlte/plugins/chart.js/Chart.min.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ url ('/adminlte/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{ url ('/adminlte/plugins/moment/moment.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ url ('/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{ url ('/adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{ url ('/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ url ('/adminlte/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ url ('/adminlte/dist/js/demo.js')}}"></script>
<!-- DataTables  & Plugins -->
<script src="{{ url ('/adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ url ('/adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{ url ('/adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, 
      "lengthChange": true, 
      "autoWidth": false,
      "scrollX": true,
      
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": false,
      "processing": true,
      "searching": true,
      "ordering": true,
      "autoWidth": true,
      "responsive": false,
      "scrollX": true,
    });

    $('#example2').on('shown.bs.collapse', function () {
     $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust();
    });
  });

    //Date picker
    $('#reservationdate1, #reservationdate2, #reservationdate3, #reservationdate4, #reservationdate5, #reservationdate6, #reservationdate7, #reservationdate8').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    function getComboA(selectObject) {
      var value = selectObject.value;

      if(value == 'Iphone' || value == 'Mobile'){
        document.getElementById('quantity_cid').disabled = false;
        document.getElementById("service").disabled = true;
        document.getElementById("service").value = "";
      } else if(value == 'MIDI'){
        document.getElementById('quantity_cid').disabled = true;
        document.getElementById("quantity_cid").value = 1;
        document.getElementById("service").disabled = false;
      } else if(value == ''){
        document.getElementById("quantity_cid").disabled = true;
        document.getElementById("service").disabled = true;
        document.getElementById("service").value = "";
      }

      console.log(value);
    }

    function getCheckbox(selectCheck) {
      var value = selectCheck.value;

      if(document.getElementById('cancel_so').checked){
        document.getElementById('notes').disabled = false;
      } else{
        document.getElementById('notes').disabled = true;
        document.getElementById("notes").value = "";
      }

      console.log(value);
    }
</script>
</body>
</html>
