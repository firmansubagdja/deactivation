@extends('layouts.master.master')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard Home</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>{{ $sr_created }}</h3>

              <p>SR Created</p>
            </div>
            <div class="icon">
              <i class="ion ion-calendar"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>{{ $so_created }}</h3>

              <p>SO Created</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>{{ $so_complete }}</h3>

              <p>SO Completed</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">

        <div class="card">
            <div class="card-header border-0">
              <h3 class="card-title">CTD ALERT</h3>
              <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-bars"></i>
                </a>
              </div>
            </div>
            <div class="card-body table-responsive p-0">
              <table class="table table-striped table-valign-middle">
                <thead>
                  <tr>
                    <th>CTD Mark</th>
                    <th>BRA</th>
                    <th>SAD</th>
                    <th>Grand Total</th>
                    <th>Kategori</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Blank</td>
                    <td>1</td>
                    <td></td>
                    <td>1</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Green</td>
                    <td></td>
                    <td>1</td>
                    <td>1</td>
                    <td>X <= -14</td>
                  </tr>
                  <tr>
                    <td>Orange</td>
                    <td></td>
                    <td>3</td>
                    <td>3</td>
                    <td>0 >= X > -7</td>
                  </tr>
                  <tr>
                    <td>Red</td>
                    <td></td>
                    <td>2</td>                  
                    <td>2</td>
                    <td>X > 0</td>
                  </tr>
                  <tr>
                    <td><b>Grand Total<b></td>
                    <td><b>1<b></td>
                    <td><b>6<b></td>
                    <td><b>7<b></td>
                    <td><b><b></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        <!-- Map card -->
        <!-- /.card -->

        <!-- solid sales graph -->
        <!-- /.card -->

      </section>
        <section class="col-lg-6 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->

          <div class="card">
            <div class="card-header border-0">
              <h3 class="card-title">Journey Customer</h3>
              <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-bars"></i>
                </a>
              </div>
            </div>
            <div class="card-body table-responsive p-0">
              <table class="table table-striped table-valign-middle">
                <thead>
                  <tr>
                    <th style="text-align:center" rowspan="2">Month SR</th>
                    <th style="text-align:center" colspan="2" rowspan="1">Cust Late Req</th>
                    <th style="text-align:center" colspan="2" rowspan="1">Cust Not Late Req</th>
                  </tr>
                  <tr>
                    <th style="text-align:center">Complete</th>
                    <th style="text-align:center">In Progress</th>
                    <th style="text-align:center">Complete</th>
                    <th style="text-align:center">In Progress</th>
                  </tr>
                </thead>
                <tbody>
                  @php $cust_late_complete = 0; @endphp
                  @php $cust_late_inprog = 0; @endphp
                  @php $cust_not_late_complete = 0; @endphp
                  @php $cust_not_late_inprog = 0; @endphp
                  @foreach($journey_customer as $val_journey_customer)
                  <tr>
                    <td>{{ $val_journey_customer->month_sr }}</td>
                    <td>{{ $val_journey_customer->cust_late_complete }}</td>
                    <td>{{ $val_journey_customer->cust_late_inprog }}</td>
                    <td>{{ $val_journey_customer->cust_not_late_complete }}</td>
                    <td>{{ $val_journey_customer->cust_not_late_inprog }}</td>
                  </tr>
                  @php $cust_late_complete+= $val_journey_customer->cust_late_complete; @endphp
                  @php $cust_late_inprog+= $val_journey_customer->cust_late_inprog; @endphp
                  @php $cust_not_late_complete+= $val_journey_customer->cust_not_late_complete; @endphp
                  @php $cust_not_late_inprog+= $val_journey_customer->cust_not_late_inprog; @endphp
                  @endforeach
                  <tr>
                    <td><b>Grand Total<b></td>
                    <td><b>{{ $cust_late_complete }}<b></td>
                    <td><b>{{ $cust_late_inprog }}<b></td>
                    <td><b>{{ $cust_not_late_complete }}<b></td>
                    <td><b>{{ $cust_not_late_inprog }}<b></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        <!-- /.card -->
      </section>
      <!-- /.Left col -->
      <!-- right col (We are only adding the ID to make the widgets sortable)-->
      <section class="col-lg-6 connectedSortable">

        <div class="card">
            <div class="card-header border-0">
              <h3 class="card-title">Activity Journey</h3>
              <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-bars"></i>
                </a>
              </div>
            </div>
            <div class="card-body table-responsive p-0">
              <table class="table table-striped table-valign-middle">
                <thead>
                  <tr>
                    <th>Month SR</th>
                    <th>SR create</th>
                    <th>SO create</th>
                    <th>SO complete</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>2021-1</td>
                    <td>144</td>
                    <td>144</td>
                    <td>144</td>
                  </tr>
                  <tr>
                    <td>2021-2</td>
                    <td>135</td>
                    <td>135</td>
                    <td>135</td>
                  </tr>
                  <tr>
                    <td>2021-3</td>
                    <td>211</td>
                    <td>211</td>
                    <td>211</td>
                  </tr>
                  <tr>
                    <td>2021-4</td>
                    <td>120</td>
                    <td>120</td>                  
                    <td>120</td>
                  </tr>
                  <tr>
                    <td>2021-5</td>
                    <td>180</td>
                    <td>179</td>
                    <td>170</td>
                  </tr>
                  <tr>
                    <td>2021-6</td>
                    <td>389</td>
                    <td>388</td>
                    <td>372</td>
                  </tr>
                  <tr>
                    <td>2021-7</td>
                    <td>61</td>
                    <td>61</td>
                    <td>8</td>
                  </tr>
                  <tr>
                    <td><b>Grand Total<b></td>
                    <td><b>1244<b></td>
                    <td><b>1238<b></td>
                    <td><b>1158<b></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        <!-- Map card -->
        <!-- /.card -->

        <!-- solid sales graph -->
        <!-- /.card -->

      </section>
      <section class="col-lg-6 connectedSortable">

        <div class="card">
            <div class="card-header border-0">
              <h3 class="card-title">Average Length Of Process (days)</h3>
              <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-bars"></i>
                </a>
              </div>
            </div>
            <div class="card-body table-responsive p-0">
              <table class="table table-striped table-valign-middle">
                <thead>
                  <tr>
                    <th>Month SR</th>
                    <th>Avg of Duration SR create</th>
                    <th>Avg of SO create</th>
                    <th>Avg of SO complete</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>2021-1</td>
                    <td>0.85</td>
                    <td>3.75</td>
                    <td>8.44</td>
                  </tr>
                  <tr>
                    <td>2021-2</td>
                    <td>2.40</td>
                    <td>3.85</td>
                    <td>12.00</td>
                  </tr>
                  <tr>
                    <td>2021-3</td>
                    <td>0.77</td>
                    <td>4.70</td>
                    <td>8.45</td>
                  </tr>
                  <tr>
                    <td>2021-4</td>
                    <td>0.94</td>
                    <td>3.79</td>                  
                    <td>16.54</td>
                  </tr>
                  <tr>
                    <td>2021-5</td>
                    <td>-0.45</td>
                    <td>2</td>
                    <td>7.52</td>
                  </tr>
                  <tr>
                    <td>2021-6</td>
                    <td>1.00</td>
                    <td>1</td>
                    <td>5.28</td>
                  </tr>
                  <tr>
                    <td>2021-7</td>
                    <td>0.64</td>
                    <td>-2</td>
                    <td>1.88</td>
                  </tr>
                  <tr>
                    <td><b>Grand Total<b></td>
                    <td><b>0.88<b></td>
                    <td><b>3<b></td>
                    <td><b>8.54<b></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        <!-- Map card -->
        <!-- /.card -->

        <!-- solid sales graph -->
        <!-- /.card -->

      </section>
      <section class="col-lg-6 connectedSortable">

        <div class="card">
            <div class="card-header border-0">
              <h3 class="card-title">Potensial Adjustment</h3>
              <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                  <i class="fas fa-bars"></i>
                </a>
              </div>
            </div>
            <div class="card-body table-responsive p-0">
              <table class="table table-striped table-valign-middle">
                <thead>
                  <tr>
                    <th>Month SR</th>
                    <th>No Impact</th>
                    <th>Not Inputed yet by BRA</th>
                    <th>Potensial Adjustment</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>2021-1</td>
                    <td>2,779,934,015</td>
                    <td></td>
                    <td>8,850,000</td>
                  </tr>
                  <tr>
                    <td>2021-2</td>
                    <td>2,213,512,602</td>
                    <td>9,000,000</td>
                    <td>12,620,000</td>
                  </tr>
                  <tr>
                    <td>2021-3</td>
                    <td>1,240,339,165</td>
                    <td></td>
                    <td>1,800,000</td>
                  </tr>
                  <tr>
                    <td>2021-4</td>
                    <td>670,497,962</td>
                    <td></td>                  
                    <td>92,000,000</td>
                  </tr>
                  <tr>
                    <td>2021-5</td>
                    <td>576,833,119</td>
                    <td>0</td>
                    <td>93,350,909</td>
                  </tr>
                  <tr>
                    <td>2021-6</td>
                    <td>1,659,486,720</td>
                    <td>0</td>
                    <td>170,194,067</td>
                  </tr>
                  <tr>
                    <td>2021-7</td>
                    <td>279,442,686</td>
                    <td></td>
                    <td>3,500,000</td>
                  </tr>
                  <tr>
                    <td><b>Grand Total<b></td>
                    <td><b>9,420,046,269<b></td>
                    <td><b>9,000,000<b></td>
                    <td><b>382,314,976<b></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        <!-- Map card -->
        <!-- /.card -->

        <!-- solid sales graph -->
        <!-- /.card -->

      </section>
      <!-- right col -->
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

</div>

@endsection