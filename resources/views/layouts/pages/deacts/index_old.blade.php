@extends('layouts.master.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tables DEACTIVATION</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered cell-border" style="width:100%">
                  <thead>
                  <tr>
                    <th >No</th>
                    <th >SR Penanda</th>
                    <th>SR Catalist</th>
                    <th>CID</th>
                    <th>Customer</th>
                    <th>CTD Date</th>
                    <th>CTD Date Assign</th>
                    <th>Request Date Sales</th>
                    <th>Request Date Customer</th>
                    <th>Product Type</th>
                    <th>Channel Request</th>
                    <th>Email Requestor</th>
                    <th>Reason Midi</th>
                    <th>Sub Reason1</th>
                    <th>Sub Reason2</th>
                    <th>Sub Reason3</th>
                    <th>Reason Validasi</th>
                    <th>PIC SR Penanda</th>
                    <th>Keterangan</th>
                    <th>SO Catalist</th>
                    <th>SO Created</th>
                    <th>Reason Catalist</th>
                    <th>CTD Update</th>
                    <th>Termination Date</th>
                    <th>Status Catalist</th>
                    <th>Completion Date</th>
                    <th>Discrepancy</th>
                    <th>Delay Completion Reason</th>
                    <th>Note</th>
                    <th>PIC SD</th>
                    <th>MRC</th>
                    <th>Month SR</th>
                    <th>CTD Alert</th>
                    <th>CTD vs Termination</th>
                    <th>Flagging CTD vs Termination</th>
                    <th>Journey Customer</th>
                    <th>Journey Sales</th>
                    <th>Delay Request</th>
                    <th>Request IOCH</th>
                    <th>SR SO Created</th>
                    <th>SO Created SO Complete</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td >2-413633890213</td>
                    <td>2-413666075321</td>
                    <td>DETH L-G-A ETR-PWT 151022</td>
                    <td>LAXO GLOBAL AKSES, PT</td>
                    <td>2021-02-28</td>
                    <td>2021-01-28</td>
                    <td>2021-01-28</td>
                    <td>2021-01-28</td>
                    <td>MIDI</td>
                    <td>EMAIL</td>
                    <td>Wachid Arif Budiman [wachid.budiman@indosatooredoo.com]</td>
                    <td>1. Harga Provider Lain Lebih Murah/Other Providers Offer</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>ENTANG HIDAYAT</td>
                    <td></td>
                    <td>2-413837778876</td>
                    <td>2021-02-01</td>
                    <td>PINDAH KE ISP LAIN</td>
                    <td>2021-02-2</td>
                    <td>2021-03-01</td>
                    <td>Complete</td>
                    <td>2021-03-01</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>9.000.000</td>
                    <td>2021-01</td>
                    <td></td>
                    <td>1</td>
                    <td>No Impact</td>
                    <td>Cust Not Late Req</td>
                    <td>0</td>
                    <td>No Delay</td>
                    <td>0</td>
                    <td>4</td>
                    <td>28</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td >2-413631381343</td>
                    <td>2-413633820064</td>
                    <td>IIPT H-3-I PTK 170012</td>
                    <td>PT. HUTCHISON 3 INDONESIA</td>
                    <td>2021-02-28</td>
                    <td>2021-01-28</td>
                    <td>2021-01-28</td>
                    <td>2021-01-28</td>
                    <td>MIDI</td>
                    <td>EMAIL</td>
                    <td>Bagus Endro Wicaksono [baguswicaksono@indosatooredoo.com]</td>
                    <td>11. Efisiensi Budget/Budget efficiency</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>ENTANG HIDAYAT</td>
                    <td></td>
                    <td>2-413673070112</td>
                    <td>2021-01-29</td>
                    <td>EFISIENSI BUDGET</td>
                    <td>2021-02-2</td>
                    <td>2021-03-01</td>
                    <td>Complete</td>
                    <td>2021-02-23</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>504.445.040</td>
                    <td>2021-01</td>
                    <td></td>
                    <td>1</td>
                    <td>No Impact</td>
                    <td>Cust Not Late Req</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>25</td>
                  </tr>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>SR Penanda</th>
                    <th>SR Catalist</th>
                    <th>CID</th>
                    <th>Customer</th>
                    <th>CTD Date</th>
                    <th>CTD Date Assign</th>
                    <th>Request Date Sales</th>
                    <th>Request Date Customer</th>
                    <th>Product Type</th>
                    <th>Channel Request</th>
                    <th>Email Requestor</th>
                    <th>Reason Midi</th>
                    <th>Sub Reason1</th>
                    <th>Sub Reason2</th>
                    <th>Sub Reason3</th>
                    <th>Reason Validasi</th>
                    <th>PIC SR Penanda</th>
                    <th>Keterangan</th>
                    <th>SO Catalist</th>
                    <th>SO Created</th>
                    <th>Reason Catalist</th>
                    <th>CTD Update</th>
                    <th>Termination Date</th>
                    <th>Status Catalist</th>
                    <th>Completion Date</th>
                    <th>Discrepancy</th>
                    <th>Delay Completion Reason</th>
                    <th>Note</th>
                    <th>PIC SD</th>
                    <th>MRC</th>
                    <th>Month SR</th>
                    <th>CTD Alert</th>
                    <th>CTD vs Termination</th>
                    <th>Flagging CTD vs Termination</th>
                    <th>Journey Customer</th>
                    <th>Journey Sales</th>
                    <th>Delay Request</th>
                    <th>Request IOCH</th>
                    <th>SR SO Created</th>
                    <th>SO Created SO Complete</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection