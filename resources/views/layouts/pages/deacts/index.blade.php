@extends('layouts.master.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tables DEACTIVATION</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin')
                 <a href="{{ route('add_deacts') }}" type="button" class="btn btn-success">Add New</a>
                @else
                 <a href="#" type="button" class="btn btn-default disabled">Add New</a>
                @endif
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover" style="width:100%">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>SR Penanda</th>
                    <th>SR Catalist</th>
                    <th>SO Number</th>
                    <th>CID</th>
                    <th>Customer</th>
                    <th>CTD Date</th>
                    <th>CTD Date Assign</th>
                    <th>Request Date Sales</th>
                    <th>Request Date Customer</th>
                    <th>Product Type</th>
                    <th>Channel Request</th>
                    <th>Email Requestor</th>
                    <th>Reason Midi</th>
                    <th>Sub Reason1</th>
                    <th>Sub Reason2</th>
                    <th>Sub Reason3</th>
                    <th>Reason Validasi</th>
                    <th>PIC SR Penanda</th>
                    <th>Keterangan</th>
                    <th>SO Catalist</th>
                    <th>SO Created</th>
                    <th>SO Submission</th>
                    <th>Reason Catalist</th>
                    <th>CTD Update</th>
                    <th>Termination Date</th>
                    <th>Status Catalist</th>
                    <th>Completion Date</th>
                    <th>Discrepancy</th>
                    <th>Delay Completion Reason</th>
                    <th>Note</th>
                    <th>PIC SD</th>
                    <th>MRC</th>
                    <th>Month SR</th>
                    <th>CTD Alert</th>
                    <th>CTD vs Termination</th>
                    <th>Flagging CTD vs Termination</th>
                    <th>Journey Customer</th>
                    <th>Journey Sales</th>
                    <th>Delay Request</th>
                    <th>Request IOCH</th>
                    <th>SR SO Created</th>
                    <th>SO Created SO Complete</th>
                    <th>CTD Mark</th>
                    <th>PIC</th>
                    <th>Aging</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $i = 0;  
                    @endphp
                    @foreach ($deacts as $deact)
                  <tr>
                    <td>{{ ++$i }}</td>
                    <td><a href="{{ url('/edit_deacts/'.$deact->id) }}">{{ $deact->sr_penanda }}</a></td>
                    <td>{{ $deact->sr_catalist }}</td>
                    <td>{{ $deact->so_number }}</td>
                    <td>{{ $deact->cid }}</td>
                    <td>{{ $deact->customer }}</td>
                    <td>{{ $deact->ctd_date }}</td>
                    <td>{{ $deact->create_date_assign }}</td>
                    <td>{{ $deact->request_date_sales }}</td>
                    <td>{{ $deact->request_date_cust }}</td>
                    <td>{{ $deact->product_type }}</td>
                    <td>{{ $deact->channel_request }}</td>
                    <td>{{ $deact->email_requestor }}</td>
                    <td>{{ $deact->reason_midi }}</td>
                    <td>{{ $deact->sub_reason1 }}</td>
                    <td>{{ $deact->sub_reason2 }}</td>
                    <td>{{ $deact->sub_reason3 }}</td>
                    <td>{{ $deact->reason_validasi }}</td>
                    <td>{{ $deact->pic_sr_penanda }}</td>
                    <td>{{ $deact->keterangan }}</td>
                    <td>{{ $deact->so_catalist }}</td>
                    <td>{{ $deact->so_created }}</td>
                    <td>{{ $deact->so_submission }}</td>
                    <td>{{ $deact->reason_catalist }}</td>
                    <td>{{ $deact->ctd_update }}</td>
                    <td>{{ $deact->termination_date }}</td>
                    <td>{{ $deact->status_catalist }}</td>
                    <td>{{ $deact->completion_date }}</td>
                    <td>{{ $deact->discrepancy }}</td>
                    <td>{{ $deact->delay_completion_reason }}</td>
                    <td>{{ $deact->note }}</td>
                    <td>{{ $deact->pic_sd }}</td>
                    <td>{{ $deact->mrc }}</td>
                    <td>{{ $deact->month_sr }}</td>
                    @if ($deact->status_catalist != "Complete")
                    @php $date = round((time() - strtotime($deact->ctd_date)) / 86400) @endphp
                    <td @if($date < 0) style="background-color: #ff4647" @endif 
                        @if($date >= 0 && $date <= 6) style="background-color: #ff892f" @endif 
                        @if($date >= 7 && $date < 14) style="background-color: #ffe12f" @endif
                        @if($date >= 14) style="background-color: #78c170" @endif>
                      <!-- {{ strtotime($deact->ctd_alert)  }} -->
                      @if ($date == -0)
                      0 Day
                      @else
                      {{ $date  }} Days
                      @endif
                    </td>
                    @else
                    <td></td>
                    @endif
                    <td>{{ $deact->ctd_vs_termination }}</td>
                    <td>{{ $deact->flagging_ctd_vs_termination }}</td>
                    <td>{{ $deact->journey_customer }}</td>
                    <td>{{ $deact->journey_sales }}</td>
                    <td>{{ $deact->delay_request }}</td>
                    <td>{{ $deact->request_ioch }}</td>
                    <td>{{ $deact->sr_so_created }}</td>
                    <td>{{ $deact->so_created_so_complete }}</td>
                    <td>{{ $deact->ctd_mark }}</td>
                    <td>{{ $deact->pic }}</td>
                    <td>{{ $deact->aging }}</td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <br>
                {{ $deacts->render() }}
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection