@extends('layouts.master.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Deactivation</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
            <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-four-sqa-tab" data-toggle="pill" href="#custom-tabs-four-sqa" role="tab" aria-controls="custom-tabs-four-sqa" aria-selected="true">SQA</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-bra-tab" data-toggle="pill" href="#custom-tabs-four-bra" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">BRA</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-sad-tab" data-toggle="pill" href="#custom-tabs-four-sad" role="tab" aria-controls="custom-tabs-four-sad" aria-selected="false">SAD</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-four-sqa" role="tabpanel" aria-labelledby="custom-tabs-four-sqa-tab">
                     <div class="row">
                         <div class="col-md-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Service Quality & Assurance</h3>
                                </div>
                                <form action="{{ url('/update_deacts/'.$deacts->id) }}" id="id" method="POST">
                                     @csrf
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>SR Penanda</label>
                                                    <input type="text" class="form-control" id="sr_penanda" name="sr_penanda" value="{{ $deacts->sr_penanda }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>SR Catalist</label>
                                                    <input type="text" class="form-control" id="sr_catalist" name="sr_catalist" value="{{ $deacts->sr_catalist }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>SO Number</label>
                                                    <input type="text" class="form-control" id="so_number" name="so_number" value="{{ $deacts->so_number }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>CID / Services ID</label>
                                                    <input type="text" class="form-control" id="cid" name="cid" value="{{ $deacts->cid }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Customer Name</label>
                                                    <input type="text" class="form-control" id="customer" name="customer" value="{{ $deacts->customer }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="bootstrreservationdateap-timepicker">
                                                    <div class="form-group">
                                                        <label>CTD Date</label>
                                                        <div class="input-group date" id="reservationdate1" data-target-input="nearest">
                                                            <input type="text" id="ctd_date" name="ctd_date" class="form-control datetimepicker-input" data-target="#reservationdate1" value="{{ $deacts->ctd_date }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                            <div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Request Date From Sales to IOCH</label>
                                                    <div class="input-group date" id="reservationdate3" data-target-input="nearest">
                                                        <input id="request_date_sales" name="request_date_sales" type="text" class="form-control datetimepicker-input" data-target="#reservationdate3" value="{{ $deacts->request_date_sales }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                        <div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Request Date From Customer</label>
                                                    <div class="input-group date" id="reservationdate4" data-target-input="nearest">
                                                        <input id="request_date_cust" name="request_date_cust" type="text" class="form-control datetimepicker-input" data-target="#reservationdate4" value="{{ $deacts->request_date_cust }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                        <div class="input-group-append" data-target="#reservationdate4" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Product Type</label>
                                                    <select class="form-control" id="product_type" name="product_type" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                      <option value="{{ $deacts->product_type }}">{{ $deacts->product_type }}</option>
                                                      <option value="">-- Select --</option>
                                                      <option value="MIDI">MIDI</option>
                                                      <option value="Iphone">Iphone</option>
                                                      <option value="Mobile">Mobile</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Channel Request</label>
                                                    <input type="text" class="form-control" id="channel_request" name="channel_request" value="{{ $deacts->channel_request }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Requester</label>
                                                    <input type="text" class="form-control" id="email_requestor" name="email_requestor" value="{{ $deacts->email_requestor }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Reason di Form Midi</label>
                                                    <input type="text" class="form-control" id="reason_midi" name="reason_midi" value="{{ $deacts->reason_midi }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Sub Reason 1</label>
                                                    <input type="text" class="form-control" id="sub_reason1" name="sub_reason1" value="{{ $deacts->sub_reason1 }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Sub Reason 2</label>
                                                    <input type="text" class="form-control" id="sub_reason2" name="sub_reason2" value="{{ $deacts->sub_reason2 }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Sub Reason 3</label>
                                                    <input type="text" class="form-control" id="sub_reason3" name="sub_reason3" value="{{ $deacts->sub_reason3 }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Reason Validasi</label>
                                                    <input type="text" class="form-control" id="reason_validasi" name="reason_validasi" value="{{ $deacts->reason_validasi }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>PIC SR Penanda</label>
                                                    <input type="text" class="form-control" id="pic_sr_penanda" name="pic_sr_penanda" value="{{ $deacts->pic_sr_penanda }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Keterangan</label>
                                                    <input type="text" class="form-control" id="keterangan" name="keterangan" value="{{ $deacts->keterangan }}" @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            @if(Auth::user()->dept == 'SQA' || Auth::user()->dept == 'admin')
                                                <button type="submit" class="btn btn-primary float-right">Submit</button>
                                            @endif
                                        </div>                                        
                                    </div>
                            </div>
                         </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-bra" role="tabpanel" aria-labelledby="custom-tabs-four-bra-tab">
                     <div class="row">
                         <div class="col-md-12">
                             <div class="card card-danger">
                                 <div class="card-header">
                                     <h3 class="card-title">BRA</h3>
                                 </div>
                                     <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>SO Catalist</label>
                                                    <input type="text" class="form-control" id="so_catalist" name="so_catalist" value="{{ $deacts->so_catalist }}" @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>SO Submission</label>
                                                        <div class="input-group date" id="reservationdate4" data-target-input="nearest">
                                                            <input id="so_created" name="so_created" type="text" class="form-control datetimepicker-input" data-target="#reservationdate4" value="{{ $deacts->so_created }}" @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                            <div class="input-group-append" data-target="#reservationdate4" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>SO Created Date</label>
                                                        <div class="input-group date" id="reservationdate5" data-target-input="nearest">
                                                            <input id="so_created" name="so_created" type="text" class="form-control datetimepicker-input" data-target="#reservationdate5" value="{{ $deacts->so_created }}" @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                            <div class="input-group-append" data-target="#reservationdate5" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Reason di Catalist</label>
                                                    <input type="text" class="form-control" id="reason_catalist" name="reason_catalist" value="{{ $deacts->reason_catalist }}" @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>CTD Update</label>
                                                        <div class="input-group date" id="reservationdate6" data-target-input="nearest">
                                                            <input id="ctd_update" name="ctd_update" type="text" class="form-control datetimepicker-input" data-target="#reservationdate6" value="{{ $deacts->ctd_update }}" @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                            <div class="input-group-append" data-target="#reservationdate6" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>Termination Date di Catalist</label>
                                                        <div class="input-group date" id="reservationdate7" data-target-input="nearest">
                                                            <input id="termination_date" name="termination_date" type="text" class="form-control datetimepicker-input" data-target="#reservationdate7" value="{{ $deacts->termination_date }}" @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                            <div class="input-group-append" data-target="#reservationdate7" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>MRC</label>
                                                    <input type="text" class="form-control" id="mrc" name="mrc" value="{{ $deacts->mrc }}" @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="form-check">
                                                      <input class="form-check-input" type="checkbox" id="cancel_so" name="cancel_so" onchange="getCheckbox(this)" @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                      <label>Cancel SO</label>
                                                    </div>
                                                    <textarea class="form-control" rows="3" id="notes_cancel" name="notes_cancel" value="{{ $deacts->notes }}" @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif disabled></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                        </div>
                                        
                                        <div class="card-footer">
                                            @if(Auth::user()->dept == 'BRA' || Auth::user()->dept == 'admin')
                                             <button type="submit" class="btn btn-primary float-right">Submit</button>
                                            @endif 
                                        </div>
                                     </div>
                             </div>
                         </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-sad" role="tabpanel" aria-labelledby="custom-tabs-four-sad-tab">
                     <div class="row">
                         <div class="col-md-12">
                             <div class="card card-success">
                                 <div class="card-header">
                                     <h3 class="card-title">SAD</h3>
                                 </div>
                                     <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Status Catalist</label>
                                                    <select class="form-control" id="status_catalist" name="status_catalist" @if(Auth::user()->dept == 'SAD' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                      <option value="{{ $deacts->status_catalist }}">{{ $deacts->status_catalist }}</option>
                                                      <option value="">-- Select --</option>
                                                      <option value="Complete">Complete</option>
                                                      <option value="In Progress">In Progress</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Completion Date di Catalist</label>
                                                        <div class="input-group date" id="reservationdate8" data-target-input="nearest">
                                                            <input id="completion_date" name="completion_date" type="text" class="form-control datetimepicker-input" data-target="#reservationdate8" value="{{ $deacts->completion_date }}" @if(Auth::user()->dept == 'SAD' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                            <div class="input-group-append" data-target="#reservationdate8" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Discrepancy</label>
                                                    <input type="text" class="form-control" id="discrepancy" name="discrepancy" value="{{ $deacts->discrepancy }}" @if(Auth::user()->dept == 'SAD' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Delayed Completion Reason</label>
                                                    <input type="text" class="form-control" id="delay_completion_reason" name="delay_completion_reason" value="{{ $deacts->delay_completion_reason }}" @if(Auth::user()->dept == 'SAD' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">NOTES</label>
                                                    <input type="text" class="form-control" id="note" name="note" value="{{ $deacts->note }}" @if(Auth::user()->dept == 'SAD' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">PIC CSD</label>
                                                    <input type="text" class="form-control" id="pic_sd" name="pic_sd" value="{{ $deacts->pic_sd }}" @if(Auth::user()->dept == 'SAD' || Auth::user()->dept == 'admin') @else disabled = "disabled" @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                          @if(Auth::user()->dept == 'SAD' || Auth::user()->dept == 'admin')
                                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                                          @endif
                                        </div>
                                     </div>
                                 </form>
                             </div>
                         </div>
                     </div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection