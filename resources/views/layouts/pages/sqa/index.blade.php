@extends('layouts.master.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DataTables SQA</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered cell-border" style="width:100%">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>SR Penanda</th>
                    <th>SR Catalist</th>
                    <th>CID</th>
                    <th>Customer</th>
                    <th>CTD Date</th>
                    <th>CTD Date Assign</th>
                    <th>Request Date Sales</th>
                    <th>Request Date Customer</th>
                    <th>Product Type</th>
                    <th>Channel Request</th>
                    <th>Email Requestor</th>
                    <th>Reason Midi</th>
                    <th>Sub Reason1</th>
                    <th>Sub Reason2</th>
                    <th>Sub Reason3</th>
                    <th>Reason Validasi</th>
                    <th>PIC SR Penanda</th>
                    <th>Keterangan</th>
                  </tr>
                  </thead>
                                    <tbody>
                    @php
                      $i = 0;  
                    @endphp
                    @foreach ($sqa as $sqa)
                  <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $sqa->sr_penanda }}</td>
                    <td>{{ $sqa->sr_catalist }}</td>
                    <td>{{ $sqa->cid }}</td>
                    <td>{{ $sqa->customer }}</td>
                    <td>{{ $sqa->ctd_date }}</td>
                    <td>{{ $sqa->create_date_assign }}</td>
                    <td>{{ $sqa->request_date_sales }}</td>
                    <td>{{ $sqa->request_date_cust }}</td>
                    <td>{{ $sqa->product_type }}</td>
                    <td>{{ $sqa->channel_request }}</td>
                    <td>{{ $sqa->email_requestor }}</td>
                    <td>{{ $sqa->reason_midi }}</td>
                    <td>{{ $sqa->sub_reason1 }}</td>
                    <td>{{ $sqa->sub_reason2 }}</td>
                    <td>{{ $sqa->sub_reason3 }}</td>
                    <td>{{ $sqa->reason_validasi }}</td>
                    <td>{{ $sqa->pic_sr_penanda }}</td>
                    <td>{{ $sqa->keterangan }}</td>
                  </tr>
                  @endforeach
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>SR Penanda</th>
                    <th>SR Catalist</th>
                    <th>CID</th>
                    <th>Customer</th>
                    <th>CTD Date</th>
                    <th>CTD Date Assign</th>
                    <th>Request Date Sales</th>
                    <th>Request Date Customer</th>
                    <th>Product Type</th>
                    <th>Channel Request</th>
                    <th>Email Requestor</th>
                    <th>Reason Midi</th>
                    <th>Sub Reason1</th>
                    <th>Sub Reason2</th>
                    <th>Sub Reason3</th>
                    <th>Reason Validasi</th>
                    <th>PIC SR Penanda</th>
                    <th>Keterangan</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection