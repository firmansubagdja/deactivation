@extends('layouts.master.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add New</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Service Quality & Assurance</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="" method="post">
                <div class="card-body">
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                				<label>SR Penanda</label>
                				<input type="text" class="form-control" id="sr_penanda" name="sr_penanda" placeholder="2-413633890213">
                			</div>
                		</div>
                		<div class="col-md-6">
                			<div class="form-group">
                				<label>SR Catalist yg diassign</label>
                				<input type="text" class="form-control" id="sr_catalist" name="sr_catalist" placeholder="2-413666075321">
                			</div>
                		</div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                				<label>CID / Services ID</label>
                				<input type="text" class="form-control" id="cid" name="cid" placeholder="DETH L-G-A ETR-PWT 151022">
                			</div>
                		</div>
                		<div class="col-md-6">
                			<div class="form-group">
                				<label>Customer Name</label>
                				<input type="text" class="form-control" id="customer" name="customer" placeholder="PT. HUTCHISON 3 INDONESIA">
                			</div>
                		</div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                			<div class="bootstrreservationdateap-timepicker">
                				<div class="form-group">
                					<label>CTD Date di Form</label>
                					<div class="input-group date" id="reservationdate" data-target-input="nearest">
                						<input type="text" id="ctd_date" name="ctd_date" class="form-control datetimepicker-input" data-target="#reservationdate">
                						<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                							<div class="input-group-text"><i class="fa fa-calendar"></i></div>
                						</div>
                					</div>
                				</div>
                			</div>
                		</div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Request Date From Sales to IOCH</label>
                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate">
                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                				<label>Request Date From Customer</label>
                				<div class="input-group date" id="reservationdate" data-target-input="nearest">
                					<input type="text" class="form-control datetimepicker-input" data-target="#reservationdate">
                					<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                						<div class="input-group-text"><i class="fa fa-calendar"></i></div>
                					</div>
                				</div>
                			</div>
                		</div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Product Type</label>
                                <input type="text" class="form-control" id="#" placeholder="MIDI">
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                				<label>Channel Request</label>
                				<input type="text" class="form-control" id="" placeholder="Channel">
                			</div>
                		</div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Requester</label>
                                <input type="text" class="form-control" id="" placeholder="Enter email">
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                				<label>Reason di Form Midi</label>
                				<input type="text" class="form-control" id="" placeholder="Enter email">
                			</div>
                		</div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Sub Reason 1</label>
                                <input type="text" class="form-control" id="" placeholder="Enter email">
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                				<label>Sub Reason 2</label>
                				<input type="text" class="form-control" id="" placeholder="Enter email">
                			</div>
                		</div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Sub Reason 3</label>
                                <input type="text" class="form-control" id="" placeholder="Enter email">
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                				<label>Reason Validasi</label>
                				<input type="text" class="form-control" id="" placeholder="Enter email">
                			</div>
                		</div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Keterangan</label>
                                <input type="text" class="form-control" id="" placeholder="Enter email">
                            </div>
                        </div>
                	</div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary float-right">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection