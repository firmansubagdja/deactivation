@extends('layouts.master.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add New</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">SAD</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form>
                <div class="card-body">
                	<div class="row">
                		<div class="col-md-6">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Month SR</label>
                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate">
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                		<div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">CTD Alert</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="2-413633890213">
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">CTD vs termination catalist</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="2-413633890213">
                            </div>
                        </div>
                		<div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Flagging CTD vs termination (<=2, OK)</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="2-413633890213">
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Journey customer (tgl request vs ctd >7hr)</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="2-413633890213">
                            </div>
                        </div>
                		<div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Journey Sales (tgl request cust vs tgl request to IOCH)</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="MIDI">
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Delay Request Deactivation</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="MIDI">
                            </div>
                        </div>
                		<div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Request to IOCH - SR create</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                            </div>
                        </div>
                	</div>
                	<div class="row">
                		<div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">SR create - SO create</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                            </div>
                        </div>
                		<div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">SO create - SO complete</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                            </div>
                        </div>
                	</div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary float-right">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection