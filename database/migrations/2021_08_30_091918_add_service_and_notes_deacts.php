<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServiceAndNotesDeacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deacts', function (Blueprint $table) {
            $table->string('service')->nullable()->after('product_type');
            $table->longText('notes_cancel')->nullable()->after('termination_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deacts', function (Blueprint $table) {
            $table->dropColumn('service');
            $table->dropColumn('notes_cancel');
        });
    }
}
