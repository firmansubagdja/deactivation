<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sr_penanda')->nullable();
            $table->string('sr_catalist')->nullable();
            $table->string('so_number')->nullable();
            $table->string('cid')->nullable();
            $table->string('customer')->nullable();
            $table->date('ctd_date')->nullable();
            $table->date('create_date_assign')->nullable();
            $table->date('request_date_sales')->nullable();
            $table->date('request_date_cust')->nullable();
            $table->string('product_type')->nullable();
            $table->string('channel_request')->nullable();
            $table->string('email_requestor')->nullable();
            $table->text('reason_midi')->nullable();
            $table->text('sub_reason1')->nullable();
            $table->text('sub_reason2')->nullable();
            $table->text('sub_reason3')->nullable();
            $table->text('reason_validasi')->nullable();
            $table->string('pic_sr_penanda')->nullable();
            $table->text('keterangan')->nullable(); 
            $table->string('so_catalist')->nullable();
            $table->date('so_created')->nullable();
            $table->string('so_submission')->nullable();
            $table->text('reason_catalist')->nullable();
            $table->date('ctd_update')->nullable();
            $table->date('termination_date')->nullable();
            $table->string('status_catalist')->nullable();
            $table->date('completion_date')->nullable();
            $table->string('discrepancy')->nullable();
            $table->text('delay_completion_reason')->nullable();
            $table->text('note')->nullable();
            $table->string('pic_sd')->nullable();
            $table->string('mrc')->nullable();
            $table->date('month_sr')->nullable();
            $table->string('ctd_alert')->nullable();
            $table->string('ctd_vs_termination')->nullable();
            $table->string('flagging_ctd_vs_termination')->nullable();
            $table->string('journey_customer')->nullable();
            $table->string('journey_sales')->nullable();
            $table->string('delay_request')->nullable();
            $table->string('request_ioch')->nullable();
            $table->string('sr_so_created')->nullable();
            $table->string('so_created_so_complete')->nullable();
            $table->string('ctd_mark')->nullable();
            $table->string('pic')->nullable();
            $table->string('aging')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deacts');
    }
}
